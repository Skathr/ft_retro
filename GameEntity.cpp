/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GameEntity.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 11:10:26 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 09:52:39 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "GameEntity.hpp"

GameEntity::GameEntity(int x, int y, const char* seq) : x_coord(x), y_coord(y), sequence(seq)
{
    draw_entity();
}

GameEntity::GameEntity()
{
    is_alive = false;
}

GameEntity::~GameEntity()
{
    mvaddstr(x_coord, y_coord, " ");
}

void    GameEntity::draw_entity()
{
    mvaddstr(x_coord, y_coord, sequence);
    refresh();
}

void GameEntity::move_up(int speed)
{
    x_coord -= speed;
    draw_entity();
}

void GameEntity::move_down(int speed)
{
    x_coord += speed;
    draw_entity();
}

void GameEntity::move_left(int speed)
{
    y_coord -= speed;
    draw_entity();
}

void GameEntity::move_right(int speed)
{
    y_coord += speed;
    draw_entity();
}

void GameEntity::kill()
{
    is_alive = false;
    mvaddch(x_coord, y_coord, ' ');
}

int GameEntity::get_x() {return x_coord;}
int GameEntity::get_y() {return y_coord;}
