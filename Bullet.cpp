/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bullet.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 12:53:37 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 08:36:46 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bullet.hpp"

Bullet::Bullet(int x, int y, char const *str) : GameEntity(x, y, str)
{
}


Bullet::Bullet() : GameEntity()
{
}

Bullet::~Bullet() {}

void Bullet::init(int x, int y, char const *str)
{
    x_coord = x;
    startX = x;
    y_coord = y;
    startY = y;
    sequence = str;
    is_alive = true;
}
