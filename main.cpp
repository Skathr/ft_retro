/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 09:36:50 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 14:03:32 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <curses.h>
#include <thread>
#include <iostream>
#include <unistd.h>
#include "Player.hpp"
#include "Enemy.hpp"
#include "Bullet.hpp"
#include "Terrain.hpp"

#define BULLETS 50
#define ENEMIES 10
#define BULLET_RANGE 300

int			move(Player & player, Bullet* bullets)
{
    int c = 0;
    int i = 0;
	switch(c = wgetch(stdscr))        
        {
            case KEY_UP:
                player.move_up(1);
                break ;
            case KEY_DOWN:
                player.move_down(1);
                break ;
            case KEY_LEFT:
                player.move_left(1);
                break ;
            case KEY_RIGHT:
                player.move_right(1);
                break ;
            case ' ':
                for(i = 0; i < BULLETS && bullets[i].is_alive == true; i++) {}
                if (i < BULLETS)
                    bullets[i].init(player.get_x(), player.get_y() + 3, "-");
                break ;
            case 27:
                 endwin();
                 return(0);
        }
        return(1);
}

void    ft_bullets(Bullet *bullets)
{
    for(int x = 0; x < BULLETS; x++)
        {
            if(bullets[x].is_alive == true)
                bullets[x].move_right(5);
            if(bullets[x].get_y() - bullets[x].startY > BULLET_RANGE)
                bullets[x].kill();
        }
}

void    ft_enemies(Enemy *enemies)
{
    for(int e = 0; e < ENEMIES; e++)
        {
            if(enemies[e].get_y() % 10 == 0 && enemies[e].bullet.is_alive == false)
                enemies[e].bullet.init(enemies[e].get_x(), enemies[e].get_y(), "*");
            if(enemies[e].bullet.get_y() < 5)
                enemies[e].bullet.is_alive = false;
            if(enemies[e].bullet.is_alive == true)
                enemies[e].bullet.move_left(5);
            if(enemies[e].is_alive == true)
                enemies[e].move_left(2);
            if(enemies[e].get_y() < -5)
                enemies[e].kill();
        }
}

int check_failure(Player & player, Enemy *enemies, int score)
{
    for(int e = 0; e < ENEMIES; e++)
        {
            if(abs(enemies[e].get_y() - player.get_y()) < 4)
                    if(enemies[e].get_x() == player.get_x())
                    {
                        endwin();
                        printf("Game Over, you scored %d\n", score);
                        return(1);
                    }
            if(abs(enemies[e].bullet.get_y() - player.get_y()) < 4)
                if(enemies[e].bullet.get_x() == player.get_x())
                {
                    endwin();
                    printf("Game Over, you scored %d\n", score);
                    return(1);
                }
        }
    return(0);
}

int score_checker(Bullet *bullets, Enemy *enemies)
{
    for(int i = 0; i < BULLETS; i++)
            for(int j = 0; j < ENEMIES; j++)
                if(abs(bullets[i].get_y() - enemies[j].get_y()) < 4)
                    if(bullets[i].get_x() == enemies[j].get_x())
                    {
                        bullets[i].kill();
                        enemies[j].kill();
                        return(1);
                    }
    return(0);
}

int main()
{
    initscr();
    curs_set(FALSE);
    noecho();
    nodelay(stdscr, TRUE);
    keypad(stdscr, TRUE);
    int score = 0;
    Player player(LINES / 2, COLS / 3, ">=>");
    Bullet *bullets = new Bullet[BULLETS];
    Enemy *enemies = new Enemy[ENEMIES];
    Terrain *terrain = new Terrain[COLS];
    for(int v = 0; v < COLS; v++)
        terrain[v].init(v);
    while(42)
    {
        clear();
        usleep(40000);
        move(0, 0);
        printw("Score: %d", score);
        if(move(player, bullets) == 0)
            return (0);
        player.draw_entity();
        for(int h = 0; h < COLS; h++)
            terrain[h].draw_entity();
        ft_bullets(bullets);
        ft_enemies(enemies);
        if(check_failure(player, enemies, score) == 1)
            return (0);
        score += score_checker(bullets, enemies);
    }
     endwin();
}
