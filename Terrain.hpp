/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Terrain.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 08:44:35 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 10:06:35 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TERRAIN_HPP
#define TERRAIN_HPP

#include "GameEntity.hpp"

class Terrain : public GameEntity
{
    public:
        Terrain();
        Terrain(Terrain const &);
        Terrain&operator=(Terrain const &);
        ~Terrain();
        int height;

        void    draw_entity();
        void    init(int x);
};

#endif