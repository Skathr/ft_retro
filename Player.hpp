/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Player.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 11:51:35 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 09:54:35 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "GameEntity.hpp"

class Player : public GameEntity
{
    public:
        Player(int x, int y, char const *str);
        Player(Player const &);
        Player & operator=(Player const &);
        ~Player();

        void move_up(int speed);
        void move_down(int speed);
        void move_left(int speed);
        void move_right(int speed);
};

#endif
