/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 15:04:19 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 12:58:09 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy( void ) : GameEntity()
{
    //srand(time(NULL));
    char const *enemies[5] = {
        "<-<",
        "<##",
        "<HH<<",
        "<---<",
        "<"
    };
    is_alive = true;
    x_coord = rand() % (LINES - 7);
    y_coord = rand() % (COLS - 1) + 200;
    sequence = enemies[rand() % 5];
}

Enemy::Enemy( char const *str ) : GameEntity()
{
    srand(time(NULL));
    x_coord = rand() % (LINES - 1);
    y_coord = rand() % (COLS - 1) + 200;
    sequence = str;
}

void Enemy::kill()
{
    x_coord = rand() % (LINES - 7);
    y_coord = rand() % (COLS - 1) + 200;
}

Enemy::~Enemy( void ) {}

/*
Enemy &		Enemy::operator=( Enemy const & rhs )
{
	this->x_coord = rhs.get_x();
	this->y_coord = rhs.get_y();
	this->sequence = rhs.sequence;
	this->is_alive = rhs.is_alive;
	return (*this);
}
*/
