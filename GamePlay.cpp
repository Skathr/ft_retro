/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GamePlay.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 18:25:36 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/10 08:38:55 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "GamePlay.hpp"

GamePlay::GamePlay( void ) : num_enemies(20), num_bullets(10)
{
	enemies = new Enemy[num_enemies];
	bullets = new Bullet[num_bullets];
	player = new Player(1, 10, ">");
}

GamePlay::GamePlay( unsigned int num_enemies, unsigned int num_bullets) : num_enemies(num_enemies),
															num_bullets(num_bullets)
{
	enemies = new Enemy[num_enemies];
	bullets = new Bullet[num_bullets];
	player = new Player(1, 10, ">");
}

GamePlay::GamePlay( GamePlay const & game )
{
	*this = game;
}

GamePlay::~GamePlay( void )
{
	delete [] bullets;
	delete [] enemies;
}

/* ******************************************************************* */
/*																	   */
/*						Getters and Setters							   */
/*																	   */
/* ******************************************************************* */

unsigned int		GamePlay::getNumBullets( void ) const
{ 
	return (num_bullets);
}

void				GamePlay::setNumBullets( unsigned int bullets )
{
	num_bullets = bullets;
}

unsigned int		GamePlay::getNumEnemies( void ) const
{
	return (num_enemies);
}

void				GamePlay::setNumEnemies( unsigned int enemies )
{
	num_enemies = enemies;
}

Enemy*				GamePlay::getEnemies( void ) const
{
	return (enemies);
}

void				GamePlay::setBullets( Enemy* const enemies )
{
	this->enemies = enemies;
}

Bullet*				GamePlay::getBullets( void ) const
{
	return (bullets);
}

void				GamePlay::setBullets( Bullet* const bullets )
{
	this->bullets = bullets;
}

Player				GamePlay::getPlayer( void ) const
{
	return (player);
}

void				GamePlay::setPlayer( Player & const player )
{
	this->player = player;
}

/* ******************************************************************* */
/*																	   */
/*						Operator Overload							   */
/*																	   */
/* ******************************************************************* */

GamePlay &			GamePlay::operator=(GamePlay const & rhs )
{
	this->num_enemies = rhs.getNumEnemies();
	this->num_bullets = rhs.getNumBullets();
	this->bullets = rhs.getBullets();
	this->player = rhs.getPlayer();
	this->enemies = rhs.getEnemies();
	return (*this);
}
