/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GamePlay.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 18:15:02 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/10 07:47:18 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_PLAY_HPP
# define GAME_PLAY_HPP

# include "Bullet.hpp"
# include "Enemy.hpp"
# include "Player.hpp"

class	GamePlay
{
	public:
		GamePlay( void );
		GamePlay( unsigned int num_enemies, unsigned int num_bullets);
		GamePlay( GamePlay const & game );
		~GamePlay( void );

		unsigned int		getNumBullets( void ) const;
		void				setNumBullets( unsigned int num_enemies );
		unsigned int		getNumEnemies( void ) const;
		void				setNumEnemies( unsigned int num_enemies );
		Enemy*				getEnemies( void ) const;
		void				setEnemies( Enemy* const enemies );
		Player				getPlayer( void ) const;
		void				setPlayer( Player const player );
		Bullet*				getBullets( void ) const;
		void				setBullets( Bullet* const player );

		GamePlay &			operator=( GamePlay const & rhs );

	private:
		Bullet*				bullets;
		Enemy*				enemies;
		Player				player;

		unsigned int		num_enemies;
		unsigned int		num_bullets;
};

#endif
