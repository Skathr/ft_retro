/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bullet.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 12:51:53 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 08:36:59 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BULLET_H
#define BULLET_H

#include "GameEntity.hpp"

class Bullet : public GameEntity
{
    public:
        Bullet();
        Bullet(int x, int y, char const *str);
        Bullet(Bullet const &);
        Bullet& operator=(Bullet const &);
        ~Bullet();

        int startX;
        int startY;
        void    init(int x, int y, char const *str);
};

#endif
