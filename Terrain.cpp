/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Terrain.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 08:46:48 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 11:19:29 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Terrain.hpp"

Terrain::Terrain() : GameEntity()
{
    
}

Terrain::~Terrain() {}

void    Terrain::init(int x)
{
    x_coord = x;
    height = LINES - (rand() % 6) - 1;
}

void    Terrain::draw_entity()
{
    if(x_coord < 0)
        x_coord += COLS;
    x_coord -= 1;
    for(int h = COLS; h > height; h--)
        mvaddstr(h, x_coord, "^");
}