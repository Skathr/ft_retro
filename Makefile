# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/09 14:52:45 by dbarrow           #+#    #+#              #
#    Updated: 2018/06/10 08:56:50 by dbarrow          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_retro

SRCS = GameEntity.cpp Player.cpp Bullet.cpp main.cpp Enemy.cpp Terrain.cpp

OBJS = GameEntity.o Player.o Bullet.o main.o Enemy.o Terrain.o

CXX = clang++

CXXFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	$(CXX) $(CXXFLAGS) -c $(SRCS)
	$(CXX) $(CXXFLAGS) $(OBJS) -o $(NAME) -lncurses
	 
re: fclean all

clean:
	/bin/rm -rf $(OBJS)

fclean: clean
	/bin/rm -rf $(NAME)
