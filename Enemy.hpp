/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 15:06:39 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 13:32:30 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
# define ENEMY_HPP

# include "GameEntity.hpp"
# include "Bullet.hpp"

class   Enemy : public GameEntity
{
    public:
        Enemy( void );
        Enemy( char const *str );
        Enemy( Enemy const & enemy);
        ~Enemy( void );

        void    kill();
        Bullet bullet;
        Enemy &     operator=( Enemy const & rhs );
};


#endif
