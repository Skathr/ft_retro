/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Player.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 11:53:32 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 09:56:19 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Player.hpp"

Player::Player(int x, int y, char const *str) : GameEntity(x, y, str)
{
    
}

Player::~Player() {}

void Player::move_up(int speed)
{
	if(x_coord < 1)
		return;
    x_coord -= speed;
    draw_entity();
}

void Player::move_down(int speed)
{
	if(x_coord > LINES - 7)
		return;
    x_coord += speed;
    draw_entity();
}

void Player::move_left(int speed)
{
	if(y_coord < 1)
		return;
    y_coord -= speed;
    draw_entity();
}

void Player::move_right(int speed)
{
	if(y_coord > COLS / 2)
		return;
    y_coord += speed;
    draw_entity();
}

/*
Player &		Player::operator=( Player const & rhs )
{
	this->x_coord = rhs.x_coord;
	this->y_coord = rhs.y_coord;
	this->is_alive = rhs.is_alive;
	this->sequence = rhs.sequence;
}*/
