/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GameEntity.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbarrow <dbarrow@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 11:04:48 by dbarrow           #+#    #+#             */
/*   Updated: 2018/06/10 09:39:19 by dbarrow          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAMEENTITY_HPP
#define GAMEENTITY_HPP

#include <curses.h>
#include <iostream>

class GameEntity
{
    protected:
        int x_coord;
        int y_coord;
        char const *sequence;

    public:
        GameEntity();
        GameEntity(int x, int y, const char* seq);
        GameEntity(GameEntity const &);
        GameEntity& operator=(GameEntity const &);
        ~GameEntity();
        int get_x();
        int get_y();
        void move_up(int speed);
        void move_down(int speed);
        void move_left(int speed);
        void move_right(int speed);
        bool is_alive;
        void kill();
        void draw_entity();
};

#endif
